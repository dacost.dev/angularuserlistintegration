import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:'home', 
    loadComponent:() => import('./user-list/user-list.component').then(mod => mod.UserListComponent)
},
{
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
